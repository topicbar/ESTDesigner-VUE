var axios = require('axios')
var MockAdapter = require('axios-mock-adapter')

import {
  groups
} from "./data/userGroups"

import {
  users
} from "./data/users"

export default {
  init() {
    var mock = new MockAdapter(axios)
    mock.onPost('/group/list').reply(200, {
      groups
    })
    mock.onPost('/user/list').reply(200, {
      users,
      total: 10
    })
  }
}
